﻿using System.Reflection;
using System.Runtime.InteropServices;

[ assembly: AssemblyTitle( "AutoMapper.Succinct" ) ]
[ assembly: AssemblyDescription( "Extension methods to make the AutoMapper syntax a little more succinct." ) ]
[ assembly: AssemblyConfiguration( "" ) ]
[ assembly: AssemblyCompany( "Tim Wilde" ) ]
[ assembly: AssemblyProduct( "AutoMapper.Succinct" ) ]
[ assembly: AssemblyCopyright( "Copyright © Tim Wilde 2014" ) ]
[ assembly: AssemblyTrademark( "" ) ]
[ assembly: AssemblyCulture( "" ) ]
[ assembly: ComVisible( false ) ]
[ assembly: Guid( "c638d0ad-44ec-4610-8e70-cd871ffcee4d" ) ]
[ assembly: AssemblyVersion( "1.0.0.0" ) ]
[ assembly: AssemblyFileVersion( "1.0.0.0" ) ]
[ assembly: AssemblyInformationalVersion( "1.0.0-alpha" ) ]
