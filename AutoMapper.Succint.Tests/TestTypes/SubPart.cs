﻿namespace AutoMapper.Succint.Tests.TestTypes
{
   public class SubPart
   {
      public string Name { get; set; }
      public string Description { get; set; }
   }
}
