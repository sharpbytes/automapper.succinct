﻿namespace AutoMapper.Succint.Tests.TestTypes
{
   public class Source
   {
      public long Id { get; set; }
      public string That { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public string Date { get; set; }
      public int Number { get; set; }
   }
}
